import config
import telebot
from dmdb import get_user, add_user, get_all_products_id, get_last_monitoring_dict, user_add_product,\
    add_product_monitoring_result, user_del_product
from dmweb import get_page, pretty_product_info, extract_product_id, create_link

bot = telebot.TeleBot(config.telegram_bot_key)


def bot_send_user_notify(chat_id, msg):
    """
    Отправка сообщения от бота к пользователю
    :param chat_id: id пользователя
    :param msg: сообщение
    :return: None
    """
    bot.send_message(chat_id, msg, parse_mode="markdown")


@bot.message_handler(commands=["start"])
def bot_start(msg):
    """
    Обработчик команды /start - инициализатора общения с ботом
    Добавить пользователя в базу если он новый, иначе вывести инфомрацию и клавиатуру управления
    :param msg: текст сообщений
    :return:
    """
    user_info = {
        'first_name': msg.chat.first_name,
        'last_name': msg.chat.last_name,
        'username': msg.chat.username,
        'id': msg.chat.id,
        'banned': config.default_banned}
    db_record = get_user(user_info['id'])
    if db_record is None:
        db_record = add_user(user_info)

    if db_record.banned:
        bot.send_message(msg.chat.id, "Извините, вы заблокированы.")
    else:
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(telebot.types.KeyboardButton("Посмотреть список отслеживаемых товаров"))
        markup.add(telebot.types.KeyboardButton("Добавить товар для отслеживания"))
        markup.add(telebot.types.KeyboardButton("Удалить товар из списка"))
        bot.send_message(
            db_record.id,
            "Здравствуйте, теперь вы можете взаимодействовать с ботом!\n\n"
            "Данный бот предназначен для мониторинга изменений интерсующих позиций магазина \"Детский мир\"\n\n"
            "Нажав на соответствующую кнопку вы можете:\n"
            " - *Посмотреть список* ваших отслеживаемых товаров\n"
            " - *Добавить товар* для мониторинга (после нажатия на кнопку отправьте ссылку на товар или ID)\n"
            " - *Удалить товар* из списка мониторинга (после нажатия на кнопку отправьте отправьте ссылку на товар ID)",
            reply_markup=markup,
            parse_mode="markdown")


@bot.message_handler(content_types=["text"])
def bot_handle_text(msg):
    """
    Обработчик сообщений от пользователя
    :param msg: сообщение
    :return: None
    """
    db_record = get_user(msg.chat.id)
    if db_record is None:  # Если пользователь новый/неизвестный - отправляем на регистрацию
        bot.send_message(msg.chat.id, "Для начала, отправьте команду /start")
        return
    elif db_record.banned:
        bot.send_message(msg.chat.id, "Извините, но вы заблокированы.")
        return

    if db_record.msg_action is None:  # Если на основной странице / не выполняем добавление/удаление
        if msg.text.startswith("Посмотреть"):
            out = ''
            if db_record.products:
                for prod_ind, product in enumerate(db_record.products):
                    last_mon = get_last_monitoring_dict(product.id)
                    out += f"[№{prod_ind + 1:0>2} {product.id}]({create_link(product.id)})\n"
                    out += f"    {product.info}\n"
                    if last_mon.get('pre_sale_price'):
                        out += f"    Цена без скидки: {last_mon['pre_sale_price']}\n"
                    out += f"    Цена: {last_mon['price']}\n"
                    best_mul = min([1.0, ]+[_['discount'] for _ in last_mon.get('promo_codes', [])])
                    for promo in last_mon.get('promo_codes', []):
                        if promo['discount'] == best_mul:
                            out += f"    Лучшая цена: {round(last_mon['price']*best_mul,2)} [[{promo['promo_word']}]]\n"
                            break
                    out += '\n'

            else:
                out = ' - Нет отслеживаемых товаров\n'
            bot.send_message(msg.chat.id,
                             f"*Список товаров*:\n{out}",
                             parse_mode="markdown",
                             disable_web_page_preview=True)
        elif msg.text.startswith("Добавить"):
            bot.send_message(msg.chat.id, "Введите ID товара / ссылку на товар")
            db_record.msg_action = 'add'
        elif msg.text.startswith("Удалить"):
            bot.send_message(msg.chat.id, "Введите ID товара / ссылку на товар")
            db_record.msg_action = 'del'
        else:
            bot.send_message(msg.chat.id, f"Извините, неизвестная команда: <{msg.text}>")
    else:
        if db_record.msg_action == 'add':
            product_id = extract_product_id(msg.text.strip())
            if product_id is None:
                bot.send_message(msg.chat.id, f"Некорректное значение: {msg.text.strip()}")
            else:
                if product_id in [_.id for _ in db_record.products]:
                    bot.send_message(msg.chat.id, f"Вы уже добавили данный товар")
                elif product_id in get_all_products_id():
                    result = get_last_monitoring_dict(product_id)
                    user_add_product(msg.chat.id, result['id'])
                    bot.send_message(msg.chat.id, "Новый товар успешно добавлен в отслеживание:\n\n" +
                                     pretty_product_info(result))
                else:
                    bot.send_message(msg.chat.id, "Ожидайте, проверяю значение...")
                    result = get_page(product_id)
                    if result.get('error'):
                        bot.send_message(msg.chat.id, f"Ошибка! Значение не добавлено: {result.get('error')}")
                    else:
                        result['user_id'] = db_record.id
                        add_product_monitoring_result(result)
                        bot.send_message(msg.chat.id, "Новый товар успешно добавлен в отслеживание:\n\n" +
                                         pretty_product_info(result))
        elif db_record.msg_action == 'del':
            product_id = extract_product_id(msg.text.strip())
            if product_id is None:
                bot.send_message(msg.chat.id, f"Некорректное значение: {msg.text.strip()}")
            else:
                if product_id in [_.id for _ in db_record.products]:
                    user_del_product(msg.chat.id, product_id)
                    bot.send_message(msg.chat.id, "Значение успешно удалено")
                else:
                    bot.send_message(msg.chat.id, f"Вы не отслеживали данный товар: {product_id}")
        db_record.msg_action = None
