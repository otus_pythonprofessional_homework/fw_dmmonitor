from dmmonitorbot import bot, bot_send_user_notify
from dmdb import update_monitoring as update_db, get_all_users
from dmweb import get_page as get_info_function
from config import update_db_interval
import threading
import time
import argparse


def start_polling():
    bot.infinity_polling(none_stop=True)


def main(updateinterval):
    polling_thread = threading.Thread(target=start_polling)
    polling_thread.start()

    while True:
        try:
            update_db(get_info_function, bot_send_user_notify)
            time.sleep(updateinterval)
        except KeyboardInterrupt:
            print("Stop program! Please wait!")
            bot.stop_polling()
            polling_thread.join()
            break
        except Exception as e:
            print(e)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='DMMonitorBot', description='dmmonitoring telegram bot')
    parser.add_argument('--interval', dest='updateinterval', type=int, default=update_db_interval, required=False, help='DB update interval in seconds')
    parser.add_argument('--check-notify', dest='checknotify', action="store_true", help='Run users notify test')
    args = parser.parse_args()
    if args.checknotify:
        for user_id in get_all_users():
            bot_send_user_notify(user_id[0], 'DMMonitorBot test notify!')
        print("Users notify test complete!")
    else:
        main(updateinterval=args.updateinterval)
