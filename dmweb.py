import re
import requests
from datetime import datetime
from lxml import html

URL_product = 'https://www.detmir.ru/product/index/id/{product_id}/'
XPATH_product_title = "//h1[@data-testid='productTitle']"
XPATH_price_block = "//section[@data-testid='priceBlock']"
XPATH_price_block_price = ".//p[@data-testid='price']"
XPATH_price_block_price_presale = "./../../p/text()"

XPATH_promo_codes_s = "./../div/h2[text() = 'Промокод']"
XPATH_promo_codes_word_s = "./../div/button[2]/span/text()"
XPATH_promo_codes_expire_s = "./../div/button[1]/span/span/div/p"
XPATH_promo_codes_info_s = "./../div/button[1]/span/span/div/h4"

XPATH_promo_codes_m = "./../div/h2[text() = 'Промокоды']"
XPATH_promo_codes_word_m = "./../div/div/div/div/div/button[2]/span/text()"
XPATH_promo_codes_expire_m = "./../div/div/div/div/div/button[1]/span/span/div/p"
XPATH_promo_codes_info_m = "./../div/div/div/div/div/button[1]/span/span/div/h4"


def get_headers() -> dict:
    """
    Получаем заголовки браузера/headers
    :return: dict
    """
    return {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/122.0.0.0 YaBrowser/24.4.0.0 Safari/537.36'}


def extract_product_id(value: str):
    """
    Извлечение product_id из переданной строки (ссылка на товар или непосредственно значение)
    :param value: строка содержаща число == product_id
    :return: product_id or None
    """
    for re_pattern in [r'detmir.ru/product/index/id/([0-9]+)', r"^(\d+)$"]:
        re_match = re.search(re_pattern, value)
        if re_match:
            return int(re_match.groups()[0])
    return None


def create_link(product_id):
    """
    Создать ссылку по product_id
    :param product_id: id товара
    :return: str
    """
    return URL_product.format(product_id=product_id)


def validate_id(product_id) -> bool:
    """
    Проверяем что полученное значение является числом
    :param product_id: значение
    :return: bool
    """
    if not re.match(r"^\d+$", f"{product_id}"):
        return False
    return True


def price_str_to_float(price) -> float:
    """
    Извлечение значения цены из строки
    :param price: строка с ценой
    :return: float
    """
    price = price.replace('\xa0₽', '')  # Удаляем символ рубля и предшествующий ему неразрывный пробел
    price = price.replace('\u2009', '')  # Удаляем символ узкого пробела
    price = price.replace(',', '.')  # Заменяем символ отделения дробной части
    return float(price)


def parse_promo_description(description: str) -> float:
    """
    Извлечение из описания промокода модификатора скидки
    :param description: строка описания промокода
    :return: float
    """
    full_price = 1.0
    if discount := re.search(r"Доп. скидка (\d+) ?% по промокоду", description, re.IGNORECASE):
        discount_value = int(discount.groups()[0])
        return full_price - discount_value / 100
    return full_price


def pretty_product_info(info: dict, out_to_tty=False):
    """
    Формирования информационного вывода из словаря (мониторинга)
    :param info: dict с данными
    :param out_to_tty: флаг вывода данных на tty::dict_call/debug
    :return: str
    """
    st_line = '/' + '=' * 79 + '\n'
    end_line = '\\' + '=' * 79 + '\n'
    output = ""
    if out_to_tty:
        output += st_line
    if info.get('id'):
        output += f'ID товара: {info.get("id")}\n'
    if info.get('datetime'):
        output += f'Время обращения: {info.get("datetime")}\n'
    if info.get('error'):
        output += f'Ошибка выполнения запроса: {info.get("error")}\n'
        if out_to_tty:
            output += end_line
        return output
    output += f'Наименование: {info.get("title")}\n'
    if not info.get('available'):
        output += f'Цена: Товара нет в наличии\n'
        if out_to_tty:
            output += end_line
        return output
    if info.get("presale_price"):
        output += f'Цена до скидки: {info.get("presale_price"):.2f} ₽\n'
    output += f'Цена: {info.get("price"):.2f} ₽\n'
    output += 'Промокоды:\n'
    if not info.get('promo_codes'):
        output += f' - Нет доступных промокодов\n'
    for promo_code in info.get('promo_codes'):
        output += f' - [{promo_code.get("promo_word")}] ({promo_code.get("promo_expire")})' \
                  f' {promo_code.get("promo_info")}\n'
        output += f'   Цена с промокодом: {round(promo_code.get("discount") * info.get("price"), 2)} ₽\n'
    if out_to_tty:
        output += end_line
    return output


def get_page(product_id) -> dict:
    """
    Получение страницы товара и извлечение из нее необходимых данных
    :param product_id: id товара
    :return: dict результата
    """
    result = {
        'id': None,  # id продукта
        'datetime': None,  # Время получения данных
        'error': None,  # Информации об ошибке
        'title': None,  # Название товара
        'available': True,  # Доступность ваора к заказу / Наличие товара
        'presale_price': None,  # Цена товара без скидки
        'price': None,  # Цена товара,
        'promo_codes': []  # Доступные промокоды для товара
    }

    if not validate_id(product_id):
        result['error'] = "Некорректное значение id (не число)"
        return result
    result['id'] = int(product_id)
    try:
        data = requests.get(create_link(product_id), headers=get_headers(), timeout=15)
        result['datetime'] = datetime.utcnow()
        if data.status_code == 404:
            result['error'] = "Товар не существует"
            return result
        if data.status_code == 418:
            result['error'] = "Ваш адрес заблокирован на сервере магазина, смените адрес"
            return  result
        elif data.status_code != 200:
            result['error'] = f"Некорректный ответ сервера: {data.status_code}"
            return result
    except Exception as e:
        result['error'] = f'[Ошибка][Не удалось получить страницу товара]: {e}'
        return result

    tree = html.fromstring(data.content)

    product_title = tree.xpath(XPATH_product_title)
    if not product_title:
        result['error'] = f"Не удалось найти поле <productTitle>"
        return result
    result['title'] = product_title[0].text_content()
    price_block = tree.xpath(XPATH_price_block)
    price = price_block[0].xpath(XPATH_price_block_price)
    if not price:
        result['available'] = False
        return result
    result['price'] = price_str_to_float(price[0].text_content())

    pre_sale_price = price[0].xpath(XPATH_price_block_price_presale)
    if pre_sale_price:
        result['presale_price'] = price_str_to_float(pre_sale_price[0])

    for m_path, w_path, i_path, e_path in [
        (XPATH_promo_codes_m, XPATH_promo_codes_word_m, XPATH_promo_codes_info_m, XPATH_promo_codes_expire_m),
        (XPATH_promo_codes_s, XPATH_promo_codes_word_s, XPATH_promo_codes_info_s, XPATH_promo_codes_expire_s)
    ]:
        promo_codes = price_block[0].xpath(m_path)
        if not promo_codes:
            continue
        for promo_word, promo_info, promo_expire in zip(promo_codes[0].xpath(w_path), promo_codes[0].xpath(i_path),
                                                        promo_codes[0].xpath(e_path)):
            promo_info = promo_info.text_content().strip()
            result['promo_codes'].append({
                'promo_word': promo_word,
                'promo_info': promo_info,
                'promo_expire': promo_expire.text_content().strip(),
                'discount': parse_promo_description(promo_info)})
    return result


def main():
    """
    Прямой вызов данного модуля
    """
    import sys
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} product_id_1 product_id_2 ...")
        return

    for product_id in sys.argv[1:]:
        product_int = extract_product_id(product_id)
        if product_int:
            print(pretty_product_info(get_page(product_int), out_to_tty=True))

    '''
    data = get_page(4539064)
    print(pretty_product_info(data, out_to_tty=True))

    # TODO: учет размеров бонусов

    # TODO: распознавание ситуации когда есть выбор опции 1/коробка
    print(pretty_product_info(get_page(511731), out_to_tty=True))

    print(pretty_product_info(get_page(6432991), out_to_tty=True))
    print(pretty_product_info(get_page(4579052), out_to_tty=True))
    print(pretty_product_info(get_page(6455526), out_to_tty=True))
    '''


if __name__ == '__main__':
    main()
