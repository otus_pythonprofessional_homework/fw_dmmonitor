from datetime import timedelta

telegram_bot_key = ""  # API ключ телеграмм бота
update_db_interval = 60 * 60 * 3  # Интервал обновления базы данных
tracking_delta = timedelta(hours=6)  # Через какое время перестать обновлять информацию о неотслеживаемых товарах
default_banned = False  # Значение banned для новых пользователей

