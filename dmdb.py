import datetime
from config import tracking_delta
from sqlalchemy import desc, create_engine, Column, Integer, DateTime, ForeignKey, Table, Boolean, Text, Float
from sqlalchemy.orm import relationship, declarative_base, sessionmaker

engine = create_engine("sqlite:///dmmonitor.db")
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

# ASSOCIATION TABLES ===================================================================================================
users_products_association = Table(
    'users_products_association',
    Base.metadata,
    Column('user_id', Integer, ForeignKey('user.id')),
    Column('product_id', Integer, ForeignKey('product.id'))
)

monitorings_promocodes_association = Table(
    'monitorings_promocodes_association',
    Base.metadata,
    Column('monitoring_id', Integer, ForeignKey('monitoring.id')),
    Column('promocode_id', Integer, ForeignKey('promocode.id'))
)


# DATA TABLES ==========================================================================================================
class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    first_name = Column(Text, nullable=True)
    last_name = Column(Text, nullable=True)
    username = Column(Text, nullable=False)
    banned = Column(Boolean, default=False)
    msg_action = Column(Text, nullable=True)
    products = relationship('Product', secondary=users_products_association, back_populates='users')


class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    info = Column(Text, nullable=True)
    last_tracking = Column(DateTime, nullable=True)
    last_update = Column(DateTime, nullable=True)
    users = relationship('User', secondary=users_products_association, back_populates='products')


class Monitoring(Base):
    __tablename__ = 'monitoring'
    id = Column(Integer, primary_key=True)
    parent_id = Column(ForeignKey("product.id"))
    check_time = Column(DateTime)
    title = Column(Text)
    available = Column(Boolean)
    pre_sale_price = Column(Float, nullable=True)
    price = Column(Float)
    promocodes = relationship('Promocode', secondary=monitorings_promocodes_association, back_populates='monitorings')


class Promocode(Base):
    __tablename__ = 'promocode'
    id = Column(Integer, primary_key=True)
    word = Column(Text, nullable=False)
    info = Column(Text, nullable=False)
    expire = Column(Text, nullable=False)
    discount = Column(Float, nullable=False)
    monitorings = relationship('Monitoring', secondary=monitorings_promocodes_association, back_populates='promocodes')


Base.metadata.create_all(bind=engine)


# DB FUNCTIONS =========================================================================================================
def get_all_users():
    """
    Получить список всех пользоватлей (для теста уведомлений)
    :return:
    """
    return session.query(User.id).all()


def add_user(user_info: dict):
    """
    Добавляем в БД нового пользователя
    :param user_info: dict с информацией о пользователе
    :return: None
    """
    new_user = User(
        id=user_info['id'],
        first_name=user_info['first_name'],
        last_name=user_info['last_name'],
        username=user_info['username'],
        banned=user_info['banned'],
        msg_action=None
    )
    session.add(new_user)
    session.commit()
    return new_user


def get_user(user_id):
    """
    Получение записи из БД о пользователе с id == user_id
    :param user_id: id пользователя
    :return: запись из БД
    """
    return session.query(User).where(User.id == user_id).first()


def user_add_product(user_id, product_id):
    """
    Добавляем отслеживание пользователем товара (пользователю user_id добавляем товар product_id)
    :param user_id: id пользователя
    :param product_id: id товара
    :return: None
    """
    user_obj = session.query(User).where(User.id == user_id).first()
    product_obj = session.query(Product).where(Product.id == product_id).first()
    if user_obj is None or product_obj is None:
        # TODO: log info
        pass
    else:
        user_obj.products.append(product_obj)
        session.commit()


def user_del_product(user_id, product_id):
    """
    Удаление отслеживания пользователем товара (у пользователя user_id удаляем товар product_id)
    :param user_id: id пользователя
    :param product_id: id товара
    :return: None
    """
    user_obj = session.query(User).where(User.id == user_id).first()
    product_obj = session.query(Product).where(Product.id == product_id).first()
    if user_obj is None or product_obj is None:
        # TODO: log info
        pass
    else:
        user_obj.products.remove(product_obj)
        if not product_obj.users:
            product_obj.last_tracking = datetime.datetime.utcnow()
        session.commit()


def get_all_products_id() -> list:
    """
    Запрос всех известных товаров
    :return: список id всех имеющихся товаров
    """
    return [_.id for _ in session.query(Product).all()]


def get_last_product_monitoring(product_id):
    """
    Получение последней (самой новой) записи мониторинга товара
    :param product_id: id товара
    :return: запись БД
    """
    return session.query(Monitoring).where(
        Monitoring.parent_id == product_id).order_by(desc(Monitoring.check_time)).first()


def monitoring_obj_to_dict(record):
    """
    Создание dict с информацией о мониторинге из записи record
    :param record: запись из БД
    :return: dict
    """
    return {
        'id': record.parent_id,
        'datetime': record.check_time,
        'title': record.title,
        'available': record.available,
        'pre_sale_price': record.pre_sale_price,
        'price': record.price,
        'promo_codes': [{
            'promo_word': _.word,
            'promo_expire': _.expire,
            'promo_info': _.info,
            'discount': _.discount} for _ in record.promocodes]
    }


def get_last_monitoring_dict(product_id):
    """
    Получение и преоб разование в dict информации о последнем мониторинге товара
    :param product_id: id товара
    :return: dict с информацией о мониторинге
    """
    return monitoring_obj_to_dict(get_last_product_monitoring(product_id))


def add_product_monitoring_result(result: dict) -> bool:
    """
    Добавление в БД данных мониторинга
    :param result: словарь с данными мониторинга
    :return: корректность данных (была ли ошибка при получении данных)
    """
    if result.get('error'):
        return False

    product = session.query(Product).where(Product.id == result['id']).first()
    if product is None:
        product = Product(
            id=result['id'],
            info=result['title'],
            last_tracking=result['datetime'],
            last_update=result['datetime'])
        session.add(product)
        session.commit()
        if result.get('user_id'):
            product.users.append(session.query(User).where(User.id == result['user_id']).first())
    product.last_update = result['datetime']
    if product.users:
        product.last_tracking = result['datetime']

    monitoring_record = Monitoring(
        parent_id=product.id,
        check_time=result['datetime'],
        title=result['title'],
        available=result['available'],
        pre_sale_price=result['presale_price'],
        price=result['price'])
    session.add(monitoring_record)
    session.commit()

    for promo_code in result.get('promo_codes'):
        # Skip promocode.expire
        # Promocode.expire == promo_code['promo_expire']).first()
        promo_obj = session.query(Promocode).where(
                Promocode.word == promo_code['promo_word'],
                Promocode.info == promo_code['promo_info']).first()
        if promo_obj is None:
            promo_obj = Promocode(
                word=promo_code['promo_word'],
                info=promo_code['promo_info'],
                expire=promo_code['promo_expire'],
                discount=promo_code['discount'])
            session.add(promo_obj)
            session.commit()
        monitoring_record.promocodes.append(promo_obj)
        session.commit()
    return True


def notify_func(product_id, notify_msg, send_notify_func):
    """
    Функция оповещения об изменениях всех пользователей отслеживающих товар
    :param product_id: id товара для которого обнаружены изменения
    :param notify_msg: текст сообщения об изменении
    :param send_notify_func: функция отправки сообщения
    :return: None
    """
    product = session.query(Product).where(Product.id == product_id).first()
    for user in product.users:
        send_notify_func(user.id, notify_msg)


def merge_monitoring(mon_res_old, mon_res_new, send_notify_func):
    """
    Сравнение двух результатов мониторинга, если есть изменения - оповещаем соответствующих пользователей
    :param mon_res_old: предыдущие данные мониторинга
    :param mon_res_new: новые данные мониторинга
    :param send_notify_func: - функция рассылки уведомлений
    :return: None
    """
    notify_msg = []
    if mon_res_old.available != mon_res_new.available:
        if mon_res_new.available:
            notify_msg.append(f' - *ДОСТУПЕН* для заказа')
        else:
            notify_func(
                product_id=mon_res_new.parent_id,
                notify_msg=f'*{mon_res_new.title}*\n - *НЕДОСТУПЕН* для заказа (нет в наличии)',
                send_notify_func=send_notify_func)
            return
    if mon_res_old.price != mon_res_new.price:
        notify_msg.append('Изменилась цена товара:')
        notify_msg.append(f'    {mon_res_old.price} >>> *{mon_res_new.price}*')
    if [_.id for _ in mon_res_old.promocodes].sort() != [_.id for _ in mon_res_new.promocodes].sort():
        notify_msg.append('Изменились доступные промокоды:')
        if not mon_res_new.promocodes:
            notify_msg.append(' - Нет доступных промокодов')
        else:
            for promo_code in mon_res_new.promocodes:
                notify_msg.append(f' - [*{promo_code.word}*] ({promo_code.expire}) {promo_code.info}')
                notify_msg.append(f'   Цена с промокодом: *{round(promo_code.discount*mon_res_new.price, 2)}*')
    if notify_msg:
        notify_msg.insert(0, f"*{mon_res_new.title}*")
        notify_func(
            product_id=mon_res_new.parent_id,
            notify_msg='\n'.join(notify_msg),
            send_notify_func=send_notify_func)


def update_monitoring(update_function, send_notify_func=None):
    """
    Обновление базы данных (получение актуальной информации об отслеживаемых позициях)
    :param update_function: - функция получения данных
    :param send_notify_func: - функция рассылки уведомлений
    :return: None
    """
    for product in session.query(Product).all():
        if product.users or tracking_delta > (datetime.datetime.utcnow()-product.last_tracking):
            result = update_function(product.id)
            success_update = add_product_monitoring_result(result)
            if success_update and send_notify_func is not None:
                monitorings = session.query(Monitoring).where(
                    Monitoring.parent_id == product.id).order_by(Monitoring.check_time).all()
                if len(monitorings) >= 2:
                    merge_monitoring(*monitorings[-2:], send_notify_func=send_notify_func)
            else:
                # TODO: log info
                pass
